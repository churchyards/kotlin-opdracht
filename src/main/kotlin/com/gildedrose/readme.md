werkwijze
---------
1. Eerst unit tests gemaakt van alle specificaties
2. Opmerkingen: 
   - de namen 'Sulfuras' en 'Backstage passes' in de specs kloppen niet met de code
   - 'aged brie' neemt twee keer toe in de code in het geval de verkoopdatum is verstreken. Dit wordt niet expliciet 
     in de spec genoemd
     
   Aangezien de code (zogenaamd) al een paar jaar in productie staat ben ik uit gegaan van de juistheid van de code, maar zou eventueel met PO besproken kunnen worden
3. Refactoring: 
   - quick win: 'Sulfuras' if-block naar boven en exit
   - simpele (en veilige) refactoring: repeating increase en decrease code in herbruikbare functions
   - risicoloze vereenvoudigingen code, zoals quality = 0
   - door het gebruik van een switch (Kotlin when) wordt de code leesbaarder.
   
   Telkens testen runnen na elke refactoring slag. De refactoring was geen doel op zich, maar na een simpel refactoring 
   stapje, werd weer een nieuwe stap zichtbaar 
4. Nadat de code na refactoring leesbaarder werd, bleek het eenvoudig om de nieuwe feature toe te voegen mbt 'conjured items'
   opmerkingen: 
   - 'conjured' zou een eigenlijk een property van Item class moeten zijn zodat aged brie eventueel ook conjured zou kunnen zijn.
   - doordat de code leesbaarder was geworden, kwam ik er achter dat conjured ook vier keer zo snel decreased indien 
     verkoopdatum is vertreken tov regulier item waarvan de verkoopdatum niet is verstreken. Mijns inziens zou met de 
     PO besproken moeten worden of dit inderdaad de bedoeling is, want de spec is hier niet expliciet over.
