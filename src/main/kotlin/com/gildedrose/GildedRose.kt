package com.gildedrose

class GildedRose(var items: Array<Item>) {

    companion object {
        val SULFURAS = "Sulfuras, Hand of Ragnaros"
        val AGED_BRIE = "Aged Brie"
        val BACKSTAGE_PASSES = "Backstage passes to a TAFKAL80ETC concert"
    }

    fun updateQuality() {
        for (i in items.indices) {
            updateItem(items[i])
        }
    }

    private fun updateItem(item: Item) {
        if (item.name.equals(SULFURAS)) return

        when(item.name) {
            AGED_BRIE -> item.increaseQualityBy(1)
            BACKSTAGE_PASSES ->
                when(item.sellIn) {
                    in 11..Int.MAX_VALUE -> item.increaseQualityBy(1)
                    in 6..10 -> item.increaseQualityBy(2)
                    in Int.MIN_VALUE..5 -> item.increaseQualityBy(3)
                }
            else -> if (item.isConjured()) item.decreaseQualityBy(2) else item.decreaseQualityBy(1)
        }

        item.sellIn -= 1

        if (item.sellIn < 0) {
            when(item.name) {
                AGED_BRIE -> item.increaseQualityBy(1)
                BACKSTAGE_PASSES -> item.quality = 0
                else -> if (item.isConjured()) item.decreaseQualityBy(2) else item.decreaseQualityBy(1)
            }
        }
    }

    fun Item.decreaseQualityBy(n: Int) = if (this.quality - n < 0) this.quality = 0 else this.quality -= n

    fun Item.increaseQualityBy(n: Int) = if (this.quality + n > 50) this.quality = 50 else this.quality += n

    fun Item.isConjured() = this.name.toUpperCase().contains("CONJURED")

}

