package com.gildedrose

import com.gildedrose.GildedRose.Companion.AGED_BRIE
import com.gildedrose.GildedRose.Companion.BACKSTAGE_PASSES
import com.gildedrose.GildedRose.Companion.SULFURAS
import javax.net.ssl.SNIHostName

class ItemObjectMother {

    companion object {
        val REGULAR_ITEM_NAME = "foo"
        val CONJURED_ITEM_NAME = "Conjured Mana Cake"
        val SELL_IN = 1
        val QUALITY = 10
        val MAX_QUALITY = 50
        val SULFURAS_QUALITY = 80
        val LONG_TERM_SELL_IN = 11
        val MEDIUM_TERM_SELL_IN = 10
        val SHORT_TERM_SELL_IN = 5

        fun regularItem() = Item(REGULAR_ITEM_NAME, SELL_IN, QUALITY)
        fun regularItemWithZeroSellIn() = Item(REGULAR_ITEM_NAME, 0, QUALITY)
        fun regularItemWithZeroQuality() = Item(REGULAR_ITEM_NAME, SELL_IN, 0)
        fun conjuredItem() = Item(CONJURED_ITEM_NAME, SELL_IN, QUALITY)
        fun conjuredItemWithZeroSellIn() = Item(CONJURED_ITEM_NAME, 0, QUALITY)
        fun agedBrieItem() = Item(AGED_BRIE, SELL_IN, QUALITY)
        fun agedBrieItemWithZeroSellIn() = Item(AGED_BRIE, 0, QUALITY)
        fun agedBrieItemWithMaximumQuality() = Item(AGED_BRIE, SELL_IN, MAX_QUALITY)
        fun sulfurasItem() = Item(SULFURAS, SELL_IN, SULFURAS_QUALITY)
        fun backstagePassesItemWithLongTermSellIn() = Item(BACKSTAGE_PASSES, LONG_TERM_SELL_IN, QUALITY)
        fun backstagePassesItemWithMediumTermSellIn() = Item(BACKSTAGE_PASSES, MEDIUM_TERM_SELL_IN, QUALITY)
        fun backstagePassesItemWithShortTermSellIn() = Item(BACKSTAGE_PASSES, SHORT_TERM_SELL_IN, QUALITY)
        fun backstagePassesItemWithZeroSellIn() = Item(BACKSTAGE_PASSES, 0, QUALITY)
    }
}
