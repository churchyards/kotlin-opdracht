package com.gildedrose

import com.gildedrose.ItemObjectMother.Companion.MAX_QUALITY
import com.gildedrose.ItemObjectMother.Companion.REGULAR_ITEM_NAME
import com.gildedrose.ItemObjectMother.Companion.QUALITY
import com.gildedrose.ItemObjectMother.Companion.SELL_IN
import com.gildedrose.ItemObjectMother.Companion.SULFURAS_QUALITY
import com.gildedrose.ItemObjectMother.Companion.agedBrieItem
import com.gildedrose.ItemObjectMother.Companion.agedBrieItemWithMaximumQuality
import com.gildedrose.ItemObjectMother.Companion.agedBrieItemWithZeroSellIn
import com.gildedrose.ItemObjectMother.Companion.backstagePassesItemWithLongTermSellIn
import com.gildedrose.ItemObjectMother.Companion.backstagePassesItemWithMediumTermSellIn
import com.gildedrose.ItemObjectMother.Companion.backstagePassesItemWithShortTermSellIn
import com.gildedrose.ItemObjectMother.Companion.backstagePassesItemWithZeroSellIn
import com.gildedrose.ItemObjectMother.Companion.conjuredItem
import com.gildedrose.ItemObjectMother.Companion.conjuredItemWithZeroSellIn
import com.gildedrose.ItemObjectMother.Companion.regularItem
import com.gildedrose.ItemObjectMother.Companion.regularItemWithZeroQuality
import com.gildedrose.ItemObjectMother.Companion.regularItemWithZeroSellIn
import com.gildedrose.ItemObjectMother.Companion.sulfurasItem
import org.junit.Assert.*
import org.junit.Test

class GildedRoseTest {

    @Test
    fun `updateQuality does not change the item name`() {
        val items = arrayOf<Item>(regularItem())
        val app = GildedRose(items)
        app.updateQuality()
        assertEquals(REGULAR_ITEM_NAME, app.items[0].name)
    }

    @Test
    fun `updateQuality lowers sellIn and quality values`() {
        val items = arrayOf<Item>(regularItem())
        val app = GildedRose(items)
        app.updateQuality()
        assertTrue(app.items[0].sellIn < SELL_IN)
        assertTrue(app.items[0].quality < QUALITY)
    }

    @Test
    fun `Once the sell by date has passed, quality degrades twice as fast`() {
        val items = arrayOf<Item>(regularItem(), regularItemWithZeroSellIn())
        val app = GildedRose(items)
        app.updateQuality()
        val diff1 = QUALITY - app.items[0].quality
        val diff2 = QUALITY - app.items[1].quality
        assertEquals(diff2, diff1 * 2)
    }

    @Test
    fun `If item is conjured, quality degrades twice as fast`() {
        val items = arrayOf<Item>(regularItem(), conjuredItem())
        val app = GildedRose(items)
        app.updateQuality()
        val diff1 = QUALITY - app.items[0].quality
        val diff2 = QUALITY - app.items[1].quality
        assertEquals(diff2, diff1 * 2)
    }

    @Test
    fun `If item is conjured, and the sell by date has passed, quality degrades four times as fast`() {
        val items = arrayOf<Item>(regularItem(), conjuredItemWithZeroSellIn())
        val app = GildedRose(items)
        app.updateQuality()
        val diff1 = QUALITY - app.items[0].quality
        val diff2 = QUALITY - app.items[1].quality
        assertEquals(diff2, diff1 * 4)
    }

    @Test
    fun `The quality of an item is never negative`() {
        val items = arrayOf<Item>(regularItemWithZeroQuality())
        val app = GildedRose(items)
        app.updateQuality()
        assertFalse(app.items[0].quality < 0)
    }

    @Test
    fun `"Aged Brie" increases in quality the older it gets`() {
        val items = arrayOf<Item>(agedBrieItem())
        val app = GildedRose(items)
        app.updateQuality()
        assertTrue(app.items[0].quality > QUALITY)
    }

    @Test
    fun `Once the sell by date has passed, quality increases twice as fast for "Aged Brie"`() {
        val items = arrayOf<Item>(agedBrieItem(), agedBrieItemWithZeroSellIn())
        val app = GildedRose(items)
        app.updateQuality()
        val diff1 = app.items[0].quality - QUALITY
        val diff2 = app.items[1].quality - QUALITY
        assertEquals(diff2, diff1 * 2)
    }

    @Test
    fun `The quality of an item is never more than 50`() {
        val items = arrayOf<Item>(agedBrieItemWithMaximumQuality())
        val app = GildedRose(items)
        app.updateQuality()
        assertTrue(app.items[0].quality <= MAX_QUALITY)
    }

    @Test
    fun `"Sulfuras, Hand of Ragnaros"", being a legendary item, never has to be sold or decreases in Quality`() {
        val items = arrayOf<Item>(sulfurasItem())
        val app = GildedRose(items)
        app.updateQuality()
        assertEquals(SELL_IN, app.items[0].sellIn)
        assertEquals(SULFURAS_QUALITY, app.items[0].quality)
    }

    @Test
    fun `"Backstage passes to a TAFKAL80ETC concert" increases in quality by 1 when sellIn is greater than 10`() {
        val items = arrayOf<Item>(backstagePassesItemWithLongTermSellIn())
        val app = GildedRose(items)
        app.updateQuality()
        assertEquals(QUALITY + 1, app.items[0].quality)
    }

    @Test
    fun `"Backstage passes to a TAFKAL80ETC concert" increases in quality by 2 when sellIn is 10 or less`() {
        val items = arrayOf<Item>(backstagePassesItemWithMediumTermSellIn())
        val app = GildedRose(items)
        app.updateQuality()
        assertEquals(QUALITY + 2, app.items[0].quality)
    }

    @Test
    fun `"Backstage passes to a TAFKAL80ETC concert" increases in quality by 3 when sellIn is 5 or less`() {
        val items = arrayOf<Item>(backstagePassesItemWithShortTermSellIn())
        val app = GildedRose(items)
        app.updateQuality()
        assertEquals(QUALITY + 3, app.items[0].quality)
    }

    @Test
    fun `"Backstage passes to a TAFKAL80ETC concert" quality value becomes 0 when sellIn is 0`() {
        val items = arrayOf<Item>(backstagePassesItemWithZeroSellIn())
        val app = GildedRose(items)
        app.updateQuality()
        assertEquals(0, app.items[0].quality)
    }

}


